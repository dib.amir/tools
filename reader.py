import pickle
import json
from functools import partial
import multiprocessing
import re

import pandas as pd
from tqdm import tqdm

from rsm.tools.iterable import parallel_sum
from rsm.tools.walker import directory_walker


class Reader(object):
    # TODO: minor refacto for walker to be passed to load_from_directory_tree
    def __init__(self, dirpath=None):
        self.walker = directory_walker(dirpath)

    @staticmethod
    def load_from_file(filepath, file_reader, **kwargs):
        file = file_reader()
        return file.read(filepath, **kwargs)

    def load_from_directory_tree(self, file_reader, **kwargs):
        files = self.walker.find_files()
        num_files = len(files)

        pool = multiprocessing.Pool()
        fixed_kwargs_reader = partial(
            Reader.load_from_file, **kwargs, file_reader=file_reader)
        results = list(
            tqdm(pool.imap(fixed_kwargs_reader, files), total=num_files))

        data = parallel_sum(sequence=results, chunksize=2).data
        return data


class file_reader(object):
    def __init__(self):
        pass

    def read(path):
        pass

    def load():
        pass

    def __add__(self, other):
        pass

    def __radd__(self, other):
        pass


class pickle_file(file_reader):

    @staticmethod
    def read(path):
        with open(path, 'rb') as input_file:
            return pickle.load(input_file)


class pandas_file(file_reader):
    def __init__(self, dataframe=None):
        if dataframe is None:
            self.data = pd.DataFrame()
        else:
            self.data = dataframe

    def read(self, filepath='', **kwargs):
        #        print('Reading ' + filepath)
        data = pd.read_csv(filepath, **kwargs)  # add index col
        data['id_train'] = re.findall('id_train=(\d+)', filepath)[0]
#        data = data.set_index(['id_train', 'code_begin'])
        self.data = data
        return self

    def __add__(self, other):
        return pandas_file(pd.concat([self.data, other.data]))

    def __radd__(self, other):
        if other == 0:
            return self
        return self.__add__(other)


def read_pickle(path):
    with open(path, 'rb') as input_file:
        return pickle.load(input_file)


def write_pickle(data, path, **kwargs):
    with open(path, 'wb') as output_file:
        pickle.dump(data, output_file, protocol=4)


def read_json(path):
    with open(path, 'r') as input_file:
        return json.load(input_file)


def write_json(data, path):
    with open(path, 'w') as output_file:
        json.dump(data, output_file)
