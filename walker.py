import os
import pdb

ROOT_DIRECTORY = '/home/ubuntu/data/anomalie_exp_partitioned/'


class directory_walker(object):

    def __init__(self, directorypath):
        self.path = directorypath

    def find_files(self):
        files_path = []
        for dirName, subdirList, fileList in os.walk(self.path):
            for fname in fileList:
                files_path += [dirName + '/' + fname]

        return files_path

    def __len__(self):
        return len(self.find_files())

    def __str__(self):
        output = ''
#        pdb.set_trace()
        print('Directory explorer: \n')
        for dirName, subdirList, fileList in os.walk(self.path):
            output += 'Found directory: {}'.format(dirName)
            output += '\n'
            print('test repr')
            for fname in fileList:
                output += '\t{}'.format(fname)
                output += '\n'
        return output


if __name__ == '__main__':
    testwalker = directory_walker(
        directorypath=ROOT_DIRECTORY)
    files = testwalker.find_files()
    print(files)
