"""
CLASS STORE to store (pickle) data (source) with an associated state
"""
from os import makedirs
from os.path import join as os_join, exists as os_exists, isfile as os_is_file
from pickle import dump, load, HIGHEST_PROTOCOL
from pandas.core.frame import DataFrame
import pandas as pd


class Store:
    """
        The store class aims to provide an interface for saving and retrieving
        data in any given format supported by the pickle.dump method.
        If a DataFrame is provided, the Dataframe's method to_pickle is used.

        The main idea behind it is that every data source is represented
        by a folder and the files in that folder are the different versions
        of that data source.

        Typically a first version of a data source will be saved in a 'raw'
        state.  Afterwards it will be saved in a 'cleaned' state.

        > store = Store('path/to/your/directory')

        > store.set('nameOfYourSource' , 'raw', yourDataHere)
        //use store.set('nameOfYourSource' , 'raw') to retrieve your raw data

        (your data cleaning occurs here)
        (...)

        > store.set('nameOfYourSource' , 'clean', yourDataHere) \
        //use store.get('nameOfYourSource', 'clean') to retrieve your cleaned data

        To ckeck all the schemas and state therein, you can use

        > print(YourInstanceOfStoreHere)

        The basepath and a dictionary containing the schemas as keys and \
        states as values will be printed.

        :Parameters:

        - `base_path` (str): the path to the directory where the pickles are to be written.\
          The directory will be created if it does not exist

        :Attributes:

        - `base_path` (str): path given as constructor's parameter

        - `schemas` (object): an object with the schema associated to the store instance

    """

    def __init__(self, base_path):
        """ Use the given base_path for all I/O operations"""
        if not os_exists(base_path):
            makedirs(base_path)
        self.base_path = base_path

        # read the file containing the dico with schemas and states here
        self.schemas = self.__load_schemas()

    def __str__(self):
        return 'Base path is: ' + self.base_path + '\n' + 'Schemas defined: ' + str(self.schemas)

    def __load_schemas(self):
        path_to_pickle = os_join(self.base_path, 'schemas.pickle')
        if os_is_file(path_to_pickle):
            with open(path_to_pickle, "rb") as file:
                return load(file)
        else:
            return {}

    def __write_schemas(self):
        path_to_pickle = os_join(self.base_path, 'schemas.pickle')
        # if not os_exists(path_to_pickle):
        # makedirs(path_to_pickle)

        with open(path_to_pickle, "wb") as file:
            dump(self.schemas, file, protocol=HIGHEST_PROTOCOL)

    def __add_schema_source(self, source, state):
        self.schemas.setdefault(source, []).append(state)

        # no duplicates
        self.schemas[source] = list(set(self.schemas[source]))
        self.__write_schemas()

    def set(self, source, state, object2pickle):
        """ Save the given data in directory.
            :param source: the name you use to identify your source data
            :param state: the state (raw, clean, etc) of the data being written
            :param object2pickle: the data to be written (can be a dataframe or \
            any object supported by the pickle.dump function)
        """

        source_path = os_join(self.base_path, source)
        full_path = os_join(source_path, state)

        if not os_exists(source_path):
            makedirs(source_path)

        if isinstance(object2pickle, DataFrame):
            print('Pickling dataframe in', full_path)
            object2pickle.to_pickle(full_path)
        else:
            with open(full_path, "wb") as file:
                print('Pickling object in', full_path)
                dump(object2pickle, file, protocol=HIGHEST_PROTOCOL)

        self.__add_schema_source(source, state)

    def get(self, source, state):
        """
            Get data corresponding to source in given state.

            :param source: the name you use to identify your source data
            :param state: the state (raw, clean, etc) of the data being written
            :return: an object (dataframe or the object previously pickled)
        """

        full_path = os_join(self.base_path, source, state)

        try:
            with open(full_path, "rb") as file:
                object2return = load(file)
        except ModuleNotFoundError:
            object2return = pd.read_pickle(full_path)
        return object2return
