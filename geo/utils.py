def getPointCoords(row, geom='geometry'):
    """Calculates coordinates ('x' or 'y') of a Point geometry"""
    return row[geom].x, row[geom].y

def getLineCoords(row, geom):
    """Returns a list of coordinates ('x' or 'y') of a LineString geometry"""
    return list( row[geom].coords.xy[0] ), list( row[geom].coords.xy[1] )

def get_coords(gdf, geom, geo_type):

    wgs84_crs = {'init': 'epsg:4326'}
    if geo_type == 'point':
        gdf = gdf.to_crs(wgs84_crs)
        gdf[['x', 'y']] = gdf.apply(getPointCoords, geom=geom, axis=1, result_type='expand')
        return gdf
    elif geo_type == 'line':
        gdf = gdf.to_crs(wgs84_crs)
        gdf[['x', 'y']] = gdf.apply(getLineCoords, geom=geom, axis=1, result_type='expand')
        return gdf
