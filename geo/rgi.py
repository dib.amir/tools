def rgi_project(point, gdf_line, gdf_pk):

    min_idx = gdf_line['geometry'].apply(lambda x:x.distance(point) ).idxmin()
    closest = gdf_line.loc[min_idx]
    code_line_closest = closest['CODE_LIGNE']
    
    
    line = closest['geometry']
    distance_along_line = line.project(point)
    
    pks = gdf_pk.query('''CODE_LIGNE==@code_line_closest''')['RK']
    pk_start = pks.min()
    pk_closest = pk_start + distance_along_line/1000
    projected_point = line.interpolate(line.project(point))
    
    return (code_line_closest, pk_closest, projected_point)
