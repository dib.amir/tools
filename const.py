fr_month_of_year = ["Janvier",
                    "Février",
                    "Mars",
                    "Avril",
                    "Mai",
                    "Juin",
                    "Juillet",
                    "Août",
                    "Septembre",
                    "Octobre",
                    "Novembre",
                    "Décembre"]

fr_day_of_week = ['Lundi', 'Mardi', 'Mercredi',
                  'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']

fr_timelabels = {'hour': 'Heure',
                 'day': 'Jour du mois',
                 'dayofweek': 'Jour de la semaine',
                 'month': 'Mois'}
