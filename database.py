import bson
import pickle
from pymongo import MongoClient
import gridfs


def get_database(database, host=None, **kwargs):
    client = MongoClient(host, **kwargs)
    return client[database]


def drop_databases(databases, host=None, **kwargs):
    client = MongoClient(host, **kwargs)
    for database in databases:
        client.drop_database(database)


def get_collection(database, collection, host=None,**kwargs):

    db = get_database(database, host=host, **kwargs)
    return db[collection]


def drop_collections(database, collections, host=None, **kwargs):
    db = get_database(database, host=host, **kwargs)
    for collection in collections:
        db[collection].drop()


def get_document(database, collection, entry=None, search_type='all', host=None, **kwargs):
    collection = get_collection(
        database, collection, host=host, **kwargs)
    if entry is None:
        return collection.find()
    elif search_type is 'one':
        return collection.find_one(entry)
    else:
        return collection.find(entry)


def remove_document(database, collection, entry=None, search_type='all', host=None, **kwargs):
    collection = get_collection(
        database, collection, host=host, **kwargs)
    if search_type is 'one':
        return collection.remove_one(entry)
    else:
        return collection.remove(entry)


def write_entry(entry, database, collection, host='mongodb://localhost:27017', **kwargs):
    collection = get_collection(database, collection, host, **kwargs)
    pid = collection.insert_one(entry).inserted_id
    return pid


def write_grid(data, database, host=None, **kwargs):
    # PICKLE ?
    db = get_database(database, host=host, **kwargs)
    fs = gridfs.GridFS(db)
    return fs.put(data)


def read_grid(database, pid, host=None, **kwargs):

    pid = pid_converter(pid)
    db = get_database(database, host=host, **kwargs)
    fs = gridfs.GridFS(db)
    return pickle.loads(fs.get(pid).read())


def split(pids):
    return pids.split(',')


def pid_converter(pid_str):
    return bson.objectid.ObjectId(pid_str)
