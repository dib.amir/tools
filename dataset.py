import pandas as pd


def multipe_to_datetime(df, keys):
    for key in keys:
        df[key] = pd.to_datetime(df[key])
    return df


def series_to_rowDF(df):
    return pd.DataFrame(columns=list(df.index.values.tolist()),
                        data=[df.values.tolist()])


def is_between(df, x, min_col, max_col):
    return (df[min_col] <= df[x]) & (df[x] < df[max_col])


def time_decomposition(df, temp_key):
    df.loc[:, 'doy'] = df[temp_key].dt.dayofyear
    df.loc[:, 'month'] = df[temp_key].dt.month
    df.loc[:, 'hour'] = df[temp_key].dt.hour + df[temp_key].dt.minute / 60
    df.loc[:, 'year'] = df[temp_key].dt.year
    return df


def drop_n_valued_columns(dataframe, n):
    columns = dataframe.columns
    columns_to_drop = columns[dataframe.nunique() <= n]
    return dataframe.drop(columns=columns_to_drop)

def binary_group_sampler(x, n_pos, n_neg):
    if x.name == 0:
        if n_pos is not False:
            return x.sample(n_neg, replace=False)
        else:
            return x
    elif x.name == 1:
        if n_neg is not False:
            return x.sample(n_pos, replace=False)
        else:
            return x
    else:
        raise ValueError('Unknown group label')
